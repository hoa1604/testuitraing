package stepdefinations;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import pages.*;

public class LoginSteps {
	WebDriver driver = null;
	LoginPage login = null;
	HomePage home = null;

	@Before
	public void openBrowser() {
		String projectPath = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", projectPath + "/src/test/resources/drivers/chromedriver.exe");
		driver = new ChromeDriver();
		login = new LoginPage(driver);
		home = new HomePage(driver);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

	}

	@Given("user is on login page")
	public void user_is_on_login_page() {
		driver.navigate().to("https://example.testproject.io/web/");
	}

	@When("^user enters (.*) and (.*)$")
	public void user_enter_username_and_password(String username, String password) {
		login.enterUserName(username);
		login.enterPassword(password);

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@And("user clicks on login")
	public void user_clicks_on_login() {

		login.clickLogin();
	}

	@Then("user is navigated to the home page")
	public void user_is_navigated_to_the_home_page() {
		boolean isLoginSuccess = home.checkLogoutIsDisplayed();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		assertEquals(true, isLoginSuccess);

	}

	@Then("^(.*) and (.*) is displayed$")
	public void error_message_is_invalid_is_displayed(String usernameError, String passwordError) {
		boolean isUsernameChecked = true;
		if (!usernameError.isEmpty()) {
			isUsernameChecked = login.usernameError().equals(usernameError) && login.usernameErrorIsDisplayed();
		}
		boolean isPasswordChecked = true;
		if (!passwordError.isEmpty()) {
			isPasswordChecked = login.passwordError().equals(passwordError) && login.passwordErrorIsDisplayed();
		}
		assertEquals(true, isUsernameChecked && isPasswordChecked);

	}

	@After
	public void closeBrowser() {
		driver.close();
		driver.quit();
	}
}
