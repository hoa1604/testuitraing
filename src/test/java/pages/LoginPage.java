package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	protected WebDriver driver;
	private By txtUserName = By.id("name");
	private By txtPassWord = By.id("password");
	private By btnLogin = By.id("login");
	private By labelUsernameError = By.cssSelector("#name + div");
	private By labelPasswordError = By.cssSelector("#passwordHelp + div");

	public LoginPage(WebDriver webDriver) {
		driver = webDriver;
	}

	public void enterUserName(String userName) {
		driver.findElement(txtUserName).sendKeys(userName);
	}

	public void enterPassword(String passWord) {
		driver.findElement(txtPassWord).sendKeys(passWord);
	}

	public void clickLogin() {
		driver.findElement(btnLogin).click();
	}

	public String usernameError() {
		return driver.findElement(labelUsernameError).getText();
	}

	public String passwordError() {
		return driver.findElement(labelPasswordError).getText();
	}

	public boolean usernameErrorIsDisplayed() {
		return driver.findElement(labelUsernameError).isDisplayed();
	}

	public boolean passwordErrorIsDisplayed() {
		return driver.findElement(labelPasswordError).isDisplayed();
	}
}
