package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {
	protected WebDriver driver;
	private By btnLogout = By.id("logout");
	
	public HomePage(WebDriver webDriver) {
		driver = webDriver;
	}
	
	public boolean checkLogoutIsDisplayed() {
		return driver.findElement(btnLogout).isDisplayed();
	}
}
