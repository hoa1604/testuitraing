package libs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExelUtils {
	private FileInputStream file;
	private XSSFWorkbook workbook;

	public void ExelUtils(String path) {
		try {
			file = new FileInputStream(new File(path));
			workbook = new XSSFWorkbook(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void readExel(String sheetName) {
		Sheet sheet = workbook.getSheet(sheetName);
		int rows = sheet.getPhysicalNumberOfRows();
		int cols = sheet.getRow(1).getPhysicalNumberOfCells();
		for (int j = 0; j < cols; j++) {
			// String columnName =
			// sheet.getRow(i).getCell(0,Row.CREATE_NULL_AS_BLANK).toString();
		}
		for (int i = 0; i < rows; i++) {
			Row row = sheet.getRow(i);

		}

	}

	private String readCellValue(Cell cell) {
		CellType cellType = cell.getCellTypeEnum();
		String value = null;
		switch (cellType) {
		case _NONE:
			break;
		case BOOLEAN:
			// value= cell.getBooleanCellValue();
			break;
		case BLANK:

			break;
		case FORMULA:
//            FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
//            value  evaluator.evaluate(cell).getNumberValue();
			break;
		case NUMERIC:
			// value = cell.getNumericCellValue();
			break;
		case STRING:
			value = cell.getStringCellValue();
			break;
		case ERROR:
			value = "!";
			break;

		}
		return value;

	}

}
