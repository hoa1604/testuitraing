package TestUiTraning.TestUiTraning;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.gherkin.*;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features", glue = {"stepdefinations"}, monochrome = true,
plugin = { "pretty","html:target/HtmlReports/report.html","json:target/JsonReports/report.json" ,"junit:target/JUnitReports/report.xml"}
)
public class RunCucumberTest {

}
