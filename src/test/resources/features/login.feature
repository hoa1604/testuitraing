Feature: Login

  Background: use in logged in
    Given user is on login page

  Scenario Outline: Check login is successful with valid credentials
    When user enters <username> and <password>
    And user clicks on login
    Then user is navigated to the home page

    Examples: 
      | username | password |
      | Hoa      |    12345 |

  Scenario Outline: Check login is fail with invalid credentials
    When user enters <username> and <password>
    And user clicks on login
    Then <usernameError> and <passwordError> is displayed

    Examples: 
      | username | password | usernameError                 | 	       |
      | Hoa      |          |                               | Password is invalid |
      |          |      123 | Please provide your full name |                     |
      | Hoa      | abc      |                               | Password is invalid |
